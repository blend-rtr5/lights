/**
 * @file    ogl.c
 * @brief   Lights in OpenGL
 * @author  Rohit Nimkar
 * @date    05/12/2023
 * @version 1.1
 */

/* Windows Header files */
#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>

/* OpenGL header files */
#include <gl/GL.h>
#include <gl/glu.h>
#include "ogl.h"

/* Macro definitions */
#define WIN_WIDTH     800U
#define WIN_HEIGHT    600U
#define UNUSED_VAL(x) ((void)x)

/* Link with OpenGl libraries */
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "GLU32.lib")

/**
 * @brief Windows Procedure callback function
 *
 * @param hwnd   [in] - Handle to window
 * @param uMsg   [in] - Message identifier
 * @param wParam [in] - word parameter
 * @param lParam [in] - Long parameter
 * @return Success or failure
 */
LRESULT CALLBACK wndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

/**
 * @brief initilize OpenGL context
 *
 * @return
 */
int initialize(void);

/**
 * @brief UnInitialize OpenGL context
 */
void uninitialize(void);
/**
 * @brief Display contexts on screen
 */
void display(void);

/**
 * @brief Update contents on screen
 */
void update(void);

/**
 * @brief toggle  full screen
 */
void ToggleFullScreen(void);
void drawObject();
/**
 * @brief Resize window
 *
 * @param width  [in] - requested width
 * @param height [in] - Requested height
 */
void resize(int width, int height);

/* Global variable declaration */
FILE*           gpFILE       = NULL;
HWND            gHwnd        = NULL; // global window handle
BOOL            gbActive     = FALSE;
DWORD           gdwStyle     = 0;                         // unsigned long 32bit
WINDOWPLACEMENT gwpPrev      = {sizeof(WINDOWPLACEMENT)}; // previous window placement
BOOL            gbFullScreen = FALSE;                     // win32s BOOL not c++ bool
HDC             ghdc         = NULL;                      // global handle for device context
HGLRC           ghrc         = NULL;                      // Global handle for rendering context
GLfloat         cAngle       = 0.0f;                      // angle of rotation

BOOL     bLight          = FALSE; // state of global lighting
GLfloat  black[4]        = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat* pLightColor     = black;
GLfloat  lightAmbient[]  = {1.0f, 1.0f, 1.0f, 1.0f};  // ambient colors of light
GLfloat  lightDiffused[] = {1.0f, 1.0f, 0.0f, 1.0f};  // diffused color of light
GLfloat  lightSpecular[] = {0.0f, 5.0f, -3.0f, 1.0f}; // Specular colors of light
GLfloat  lightPosition[] = {-4.0f, 0.0f, 0.0f, 1.0f}; // position of light

GLfloat materialAmbient[]  = {1.0f, 1.0f, 1.0f, 1.0f}; // ambient colors of light
GLfloat materialDiffused[] = {1.0f, 1.0f, 1.0f, 1.0f}; // diffused color of light
GLfloat materialSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f}; // Specular colors of light
GLfloat materialEmission[] = {1.0f, 1.0f, 0.0f, 1.0f}; // Specular colors of light

BOOL    bModel                 = FALSE;                    // state of light model
GLfloat cosmisAmbient[]        = {0.0f, 0.0f, 1.0f, 1.0f}; // ambient color of cosmic background radiation
GLfloat cosmisAmbientDefault[] = {1.0f, 0.0f, 0.0f, 1.0f}; // ambient color of cosmic background radiation
GLfloat temp[4]                = {0};                      // placeholder to get data from openGL

GLuint      uTexture = 0U;
BOOL        bTexture = FALSE;
GLUquadric* pQuadric = NULL;
/**
 * @brief Entry point function for Win32 Desktop application
 *
 * @param hInstance    [in] - instance of current program
 * @param hPreInstance [in] - always null (except win 3.0)
 * @param lpszCmdLine  [in] - command line parameters
 * @param iCmdShow     [in] - SW_SHOWMINNOACTIVE or SW_SHOWNORMAL
 *
 * @return return value from the application
 */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPreInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    /* Local variables */
    WNDCLASSEX wndclass             = {0};                              // window class typw of window , EX for extended
    HWND       hwnd                 = NULL;                             // handle though it uint_t* its uint only bcoz can't dereference it. opaque ptr
    MSG        msg                  = {0};                              // message struct
    TCHAR      szAppName[]          = TEXT("OpenGL Window Class Name"); // unique name for class
    int        xDesktopWindowWidth  = 0;                                // width of desktop window in pixels
    int        yDesktopWindowHeight = 0;                                // height of desktop window in pixels
    int        x                    = 0;                                // window leftmost point
    int        y                    = 0;                                // window top point
    int        iResult              = 0;                                // result of operations
    BOOL       bDone                = FALSE;                            // flag to indicate message loop compuletion status

    gpFILE = fopen("Log.txt", "w");
    if (NULL == gpFILE)
    {
        MessageBox(NULL, TEXT("Log file can not be opened"), TEXT("ERROR"), MB_OK | MB_ICONERROR);
        exit(0);
    }
    fprintf(gpFILE, "Program for white triangle rotation started successfully!!!!\n");

    // WNDCLASSEX wndclass initialization predefined classes => button,dialog,scrollbar,status,dynamic
    wndclass.cbSize      = sizeof(WNDCLASSEX);                    // count of bytes in WNDCLASSEX
    wndclass.style       = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;    // redraw after resize (class style)
    wndclass.cbClsExtra  = 0;                                     // count of extra bytes in Class
    wndclass.cbWndExtra  = 0;                                     // count of extra bytes in window
    wndclass.lpfnWndProc = wndProc;                               // windows procedure callback function
    wndclass.hInstance   = hInstance;                             // give current program instance to handl
    wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH); // gdi32.dll handle OF Stock brush from os  (getstockobjext => brush,font,paint ret val HGDIIBJ => handle graphic device interface obj )
    wndclass.hIcon         = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON)); // to give our icon to our window..make resource from given int which is in rc file
    wndclass.hCursor       = LoadCursor(NULL, IDC_ARROW);                  // load predefined cursor provided by OS
    wndclass.lpszClassName = szAppName;                                    // in custom class classname is mandatory to assign
    wndclass.lpszMenuName  = NULL;                                         // menu name
    wndclass.hIconSm       = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON)); // sm = small icon to display icon on taskbar  (new one)

    /* Register class with OS -> os creates unique immutable string for this class of type ATOM */
    (void)RegisterClassEx(&wndclass);

    xDesktopWindowWidth  = GetSystemMetrics(SM_CXSCREEN);
    yDesktopWindowHeight = GetSystemMetrics(SM_CYSCREEN);

    x = (xDesktopWindowWidth - 800) / 2;
    y = (yDesktopWindowHeight - 600) / 2;

    /* create window */
    hwnd = CreateWindowEx(
        WS_EX_APPWINDOW, szAppName, TEXT("Rohit Nimkar RTR5"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, x, //(type of window name, Caption bar name, style of window => on
                                                                                                                                        // over all windows, where to show window onn desktop x,
        y, WIN_WIDTH, WIN_HEIGHT, NULL, // y,width,height set to defaults, whose child window is this default => os's child window or HWND_DESKTOP can be written
        NULL, hInstance, NULL);         // handle to menu, instance of whose window is to create, window msg after create so can be pass to wndproc through this param

    // style of window => 6types = WS_OVERLAPPPED | WS_CAPTION | WS_SYSMENU (move/resize..sys bcoz for all windows) | WS_THICKFRAME |WS_NAXIMIZEBOX | WS_MINIMIZEBOX
    gHwnd = hwnd;

    // initialization
    iResult = initialize();
    if (iResult != 0)
    {
        MessageBox(hwnd, TEXT("initialize() failed"), TEXT("ERROR"), MB_OK | MB_ICONERROR);
        DestroyWindow(hwnd);
    }

    /* to show window (handle of window, how to display window => default icmshow */
    ShowWindow(hwnd, iCmdShow);

    SetForegroundWindow(hwnd); // sets window on foreground irrespective of children/sibling window pos
    SetFocus(hwnd);            // keeps window highlighted

    /* game loop */
    while (bDone == FALSE)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
                bDone = TRUE;
            else
            {
                UNUSED_VAL(TranslateMessage(&msg));
                UNUSED_VAL(DispatchMessage(&msg));
            }
        }
        else
        {
            if (gbActive == TRUE)
            {
                /* If window is focused then display the window and update state */
                display();
                update();
            }
        }
    }

    /* clean up */
    uninitialize();

    return (int)msg.wParam;
}

LRESULT CALLBACK wndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    switch (iMsg)
    {
        case WM_SETFOCUS: /* arrives when window is activated */
        {
            gbActive = TRUE;
            break;
        }
        case WM_KILLFOCUS: /* arrives when other window is activated */
        {
            gbActive = FALSE;
            break;
        }
        case WM_SIZE: /* arrives when window is resized */
        {
            resize(LOWORD(lParam), HIWORD(lParam));
            break;
        }
        case WM_KEYDOWN: /* arrives when non char key is pressed */
        {
            switch (LOWORD(wParam))
            {
                case VK_ESCAPE: /* arrives when escapekey is pressed */
                    /*
                        Deactivate window, remove keyboard focus
                        Flush thread messages queue
                        Destroy Menu and timers

                        If Parent then destroy child windows first
                    */
                    UNUSED_VAL(DestroyWindow(hwnd));
            }
            break;
        }
        case WM_CHAR: /* arrives when char keys are pressed */
        {
            switch (LOWORD(wParam))
            {
                case 'F':
                case 'f':
                    if (gbFullScreen == FALSE)
                    {
                        ToggleFullScreen();
                        gbFullScreen = TRUE;
                        fprintf(gpFILE, "F/f key pressed and entered into full screen successfully!!!!\n");
                        break;
                    }
                    else
                    {
                        ToggleFullScreen();
                        gbFullScreen = FALSE;
                        fprintf(gpFILE, "F/f key pressed and exited into full screen successfully!!!!\n");
                        break;
                    }
                    break;
                case 'l':
                case 'L':
                    if (bLight)
                    {
                        glDisable(GL_LIGHTING);
                        bLight = FALSE;
                    }
                    else
                    {
                        glEnable(GL_LIGHTING);
                        // glLightModelfv(GL_LIGHT_MODEL_AMBIENT, cosmisAmbient);
                        bLight = TRUE;
                    }
                    break;
                case 'M':
                case 'm':
                    if (bModel)
                    {
                        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, cosmisAmbientDefault);
                        bModel = FALSE;
                    }
                    else
                    {
                        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, cosmisAmbient);
                        bModel = TRUE;
                    }
                    break;
                case VK_NUMPAD0:
                case '0':
                    static bLight0 = FALSE;
                    if (bLight0)
                    {
                        glBindTexture(GL_TEXTURE_2D, 0);
                        pLightColor = black;
                        glDisable(GL_LIGHT0);
                        bLight0 = FALSE;
                    }
                    else
                    {
                        pLightColor = lightDiffused;
                        glEnable(GL_LIGHT0);
                        bLight0 = TRUE;
                    }
                    break;

                case 't':
                case 'T': bTexture = !bTexture; break;
            }
            break;
        }
        case WM_CLOSE:
        {
            // optional if need to run program though window is closed
            UNUSED_VAL(DestroyWindow(hwnd));
            break;
        }
        case WM_DESTROY: // compulsory to handle as unhandled left window alive though prog terminates
        {
            PostQuitMessage(0);
            break;
        }
        default: break;
    }
    return (DefWindowProc(hwnd, iMsg, wParam, lParam)); // default window procedure of os which handles other msges than handles by wndproc
}

void ToggleFullScreen(void)
{
    MONITORINFO mInfo = {sizeof(MONITORINFO)};

    if (gbFullScreen == FALSE)
    {
        gdwStyle = GetWindowLong(gHwnd, GWL_STYLE);

        if (gdwStyle & WS_OVERLAPPEDWINDOW)
        {
            if (GetWindowPlacement(gHwnd, &gwpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mInfo))
            {
                UNUSED_VAL(SetWindowLong(gHwnd, GWL_STYLE, (gdwStyle & ~WS_OVERLAPPEDWINDOW)));
                UNUSED_VAL(SetWindowPos(
                    gHwnd, HWND_TOP, mInfo.rcMonitor.left, mInfo.rcMonitor.top, (mInfo.rcMonitor.right - mInfo.rcMonitor.left), (mInfo.rcMonitor.bottom - mInfo.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED));
            }
        }

        UNUSED_VAL(ShowCursor(FALSE));
    }
    else
    {
        UNUSED_VAL(SetWindowPlacement(gHwnd, &gwpPrev));
        UNUSED_VAL(SetWindowLong(gHwnd, GWL_STYLE, (gdwStyle | WS_OVERLAPPEDWINDOW)));
        UNUSED_VAL(SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED));
        UNUSED_VAL(ShowCursor(TRUE));
    }
}
BOOL loadGLTexture(GLuint* texture, TCHAR ImageResourceID[])
{
    HBITMAP hBitmap = NULL;
    BITMAP  bmp     = {0};

    /* Load texture image */
    hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), ImageResourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
    if (hBitmap == NULL)
    {
        fprintf(gpFILE, "lOAD IMAGE() failed\n");
        return FALSE;
    }

    /* Extract image data */
    GetObject(hBitmap, sizeof(BITMAP), &bmp);

    /* Create OpenGL Texture Object */
    glGenTextures(1, texture);

    /* Bind Generated texture */
    glBindTexture(GL_TEXTURE_2D, *texture);

    /* alignment and unpacking */
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

    /* Set Texture parameters */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    /* build mipmap images */
    gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, (void*)bmp.bmBits);

    /* Unbind texture */
    glBindTexture(GL_TEXTURE_2D, 0);

    /* Delete image resource */
    DeleteObject(hBitmap);

    hBitmap = NULL;

    return TRUE;
}

int initialize(void)
{
    PIXELFORMATDESCRIPTOR pfd               = {0};
    int                   iPixelFormatIndex = 0;
    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

    // initialization of pixelformat descriptor
    pfd.nSize      = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion   = 1;
    pfd.dwFlags    = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // draw on window woth real time
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cRedBits   = 8;
    pfd.cBlueBits  = 8;
    pfd.cGreenBits = 8;
    pfd.cAlphaBits = 8;
    pfd.cDepthBits = 32;

    ghdc = GetDC(gHwnd);
    if (ghdc == NULL)
    {
        fprintf(gpFILE, "GetDC() failed\n");
        return -1;
    }

    /* Step 3 */
    iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
    // should return non-zero positive value on success
    if (0 == iPixelFormatIndex)
    {
        fprintf(gpFILE, "ChoosePizelFormat() failed\n");
        return -2;
    }

    /* Step 4 : set obtained pixel format */
    if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
    {
        fprintf(gpFILE, "SetPixelFormat() failed\n");
        return -3;
    }

    /* Step 5 : create opengl contexet from device context */
    // windows opengl bridging api => WGL windows graphics library
    // tell WGL  to give me opengl compatible dc
    // tell WGL  to give me opengl compatible context from this dc

    ghrc = wglCreateContext(ghdc);
    if (ghrc == NULL)
    {
        fprintf(gpFILE, "wglCreateContext() failed\n");
        return -4;
    }
    //(now ghdc will give up its control and gave it to ghrc)

    /* Step 6: make rendering context current */
    if (wglMakeCurrent(ghdc, ghrc) == FALSE)
    {
        fprintf(gpFILE, "wglMakeCurrent() failed\n");
        return -5;
    }
    /* Drawing control is now with OpenGL */
    BOOL bResult = loadGLTexture(&uTexture, MAKEINTRESOURCE(KUNDALIBITMAP));
    if (bResult == FALSE)
    {
        fprintf(gpFILE, "loading of kundali texture failed\n");
        return -7;
    }

    /* Enable texture */
    glEnable(GL_TEXTURE_2D);
    /* Enabling Deptah */
    glShadeModel(GL_SMOOTH);                           //[Optional/Beautification] If lighting and coloring is enabled then make the shades smooth
    glClearDepth(1.0f);                                //[Compulsory] Make all bits in depth buffer as '1'
    glEnable(GL_DEPTH_TEST);                           //[Compulsory] enable depth test
    glDepthFunc(GL_LEQUAL);                            //[Compulsory] Which function to use for testing
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // [Optional/Beautification] If corner circular objects are appearing as elliptical then correct that

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // set the state, will be applied using glClear()

    /* Ambient RGBA Lighting of entire scene */
    glGetFloatv(GL_LIGHT_MODEL_AMBIENT, cosmisAmbientDefault);
    fprintf(gpFILE, "[Initialize] [Cosmic Background Radiation] Ambient => (%.2f, %.2f, %.2f, %.2f)\n", cosmisAmbientDefault[0], cosmisAmbientDefault[1], cosmisAmbientDefault[2], cosmisAmbientDefault[3]);

    /* How specular reflectin angles are computed */
    glGetFloatv(GL_LIGHT_MODEL_LOCAL_VIEWER, temp);
    fprintf(gpFILE, "[Initialize] [Cosmic Background Radiation] Local Viewer => (%.2f)\n", temp[0]);

    /* isOneside of Tow sided lighting? */
    glGetFloatv(GL_LIGHT_MODEL_TWO_SIDE, temp);
    fprintf(gpFILE, "[Initialize] [Cosmic Background Radiation] Two Side => (%.2f)\n", temp[0]);

    glGetMaterialfv(GL_FRONT, GL_AMBIENT, temp);
    fprintf(gpFILE, "[Initialize] [Material Front] Ambient Reflectance => (%.2f, %.2f, %.2f, %.2f)\n", temp[0], temp[1], temp[2], temp[3]);
    glGetMaterialfv(GL_FRONT, GL_DIFFUSE, temp);
    fprintf(gpFILE, "[Initialize] [Material Front] Diffuse Reflectance => (%.2f, %.2f, %.2f, %.2f)\n", temp[0], temp[1], temp[2], temp[3]);
    glGetMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, temp);
    fprintf(gpFILE, "[Initialize] [Material Front] Ambient & Diffuse Reflectance => (%.2f, %.2f, %.2f, %.2f)\n", temp[0], temp[1], temp[2], temp[3]);
    glGetMaterialfv(GL_FRONT, GL_SPECULAR, temp);
    fprintf(gpFILE, "[Initialize] [Material Front] Specular Reflectance => (%.2f, %.2f, %.2f, %.2f)\n", temp[0], temp[1], temp[2], temp[3]);

    /* Appears like emitting color, does notactually emit color */
    glGetMaterialfv(GL_FRONT, GL_EMISSION, temp);
    fprintf(gpFILE, "[Initialize] [Material Front] Emission => (%.2f, %.2f, %.2f, %.2f)\n", temp[0], temp[1], temp[2], temp[3]);

    /* concentration of highlight of Specular component */
    glGetMaterialfv(GL_FRONT, GL_SHININESS, temp);
    fprintf(gpFILE, "[Initialize] [Material Front] Shininess => (%.2f)\n", temp[0]);

    fprintf(gpFILE, "[Initialize] [Lighting] isEnabled => (%hhd)\n", glIsEnabled(GL_LIGHTING));

    for (UINT32 idx = 0U; idx < 8; idx++)
    {
        fprintf(gpFILE, "[Initialize] [Light%d] isEnabled => (%hhd)\n", idx, glIsEnabled(GL_LIGHT0 + idx));

        glGetLightfv(GL_LIGHT0 + idx, GL_AMBIENT, temp);
        fprintf(gpFILE, "[Initialize] [Light%d] Ambient => (%.2f, %.2f, %.2f, %.2f)\n", idx, temp[0], temp[1], temp[2], temp[3]);

        glGetLightfv(GL_LIGHT0 + idx, GL_DIFFUSE, temp);
        fprintf(gpFILE, "[Initialize] [Light%d] Diffuse => (%.2f, %.2f, %.2f, %.2f)\n", idx, temp[0], temp[1], temp[2], temp[3]);

        glGetLightfv(GL_LIGHT0 + idx, GL_SPECULAR, temp);
        fprintf(gpFILE, "[Initialize] [Light%d] Specular => (%.2f, %.2f, %.2f, %.2f)\n", idx, temp[0], temp[1], temp[2], temp[3]);

        glGetLightfv(GL_LIGHT0 + idx, GL_POSITION, temp);
        fprintf(gpFILE, "[Initialize] [Light%d] Position => (%.2f, %.2f, %.2f, %.2f)\n", idx, temp[0], temp[1], temp[2], temp[3]);

        glGetLightfv(GL_LIGHT0 + idx, GL_SPOT_DIRECTION, temp);
        fprintf(gpFILE, "[Initialize] [Light%d] Spot Direction => (%.2f, %.2f, %.2f, %.2f)\n", idx, temp[0], temp[1], temp[2], temp[3]);

        glGetLightfv(GL_LIGHT0 + idx, GL_SPOT_CUTOFF, temp);
        fprintf(gpFILE, "[Initialize] [Light%d] Spot Cutoff => (%.2f)\n", idx, temp[0]);

        glGetLightfv(GL_LIGHT0 + idx, GL_SPOT_EXPONENT, temp);
        fprintf(gpFILE, "[Initialize] [Light%d] Spot Exponent => (%.2f)\n\n", idx, temp[0]);
    }

    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffused);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

    /* Initialize quadric */
    pQuadric = gluNewQuadric();
    // internally sets the color buffer bit
    resize(WIN_WIDTH, WIN_HEIGHT);

    return 0;
}

void resize(int width, int height)
{
    if (height <= 0)
        height = 1;

    glMatrixMode(GL_PROJECTION);                       // for matrix calculation while resizing use GL_PROJECTION
    glLoadIdentity();                                  // take identity matrix for beginning
    glViewport(0, 0, (GLsizei)width, (GLsizei)height); // bioscope/Binoculor => focus on which are to be see in window => here we telling to focus on whole window
    gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    gluLookAt(0.0, 0.0, 8.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}

void display(void)
{
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the window with color whose bit is set, all bits in depth buffer set to 1 (if value is 1 or lower because of LEQUAL)
    glMatrixMode(GL_MODELVIEW);                         // for matrix calculation while displaying use GL_MODELVIEW beacuse need to display something
    glLoadIdentity();                                   // take identity matrix for beginning

    glPushMatrix();
    glTranslatef(2.0f, 0.0f, -6.0f);

    /* If all axes are rotated in single call, then opengl will rotate any randomly chosen axis, to ensure rotation on all 3 axes, use 3 separate calls as follows */
    glRotatef(cAngle, 1.0f, 1.0f, 1.0f);

    drawObject();
    glPopMatrix();

    glTranslatef(lightPosition[0], lightPosition[1], lightPosition[2]);
    glMaterialfv(GL_FRONT, GL_EMISSION, pLightColor);
    gluSphere(pQuadric, 0.2f, 20, 20); // it will create all normals for you
    glMaterialfv(GL_FRONT, GL_EMISSION, black);
    SwapBuffers(ghdc); // as in pfd.dwFlags = PFD_DOUBLEBUFFER
    // ghrc is for=> opengl & ghdc is for=> OS
}

void update(void)
{
    /* Update states */
    cAngle = cAngle - 2.0f; // even += is better we do not use shorthand

    if (cAngle <= 0.0f)
    {
        cAngle = cAngle + 360.0f;
    }
}

void uninitialize(void)
{
    if (NULL != pQuadric)
    {
        gluDeleteQuadric(pQuadric);
        pQuadric = NULL;
    }

    if (TRUE == gbFullScreen)
    {
        ToggleFullScreen();
        gbFullScreen = FALSE;
    }

    /* reset rendering context */
    if (wglGetCurrentContext() == ghrc) // if current context is ghrc  then make current context as NULL
    {
        wglMakeCurrent(NULL, NULL); // make current context NULL
    }

    /* Delete OpenGL rendering context */
    if (NULL != ghrc)
    {
        wglDeleteContext(ghrc);
        ghrc = NULL;
    }

    /* release global dc */
    if (NULL != ghdc)
    {
        ReleaseDC(gHwnd, ghdc); // gHwnd => whose windows ghdc =>which ghdc
        ghdc = NULL;
    }

    /* Destroy window */
    if (NULL != gHwnd)
    {
        DestroyWindow(gHwnd);
        gHwnd = NULL;
    }
    if (0U != uTexture)
    {
        glDeleteTextures(1, &uTexture);
        uTexture = 0U;
    }

    /* Close logging file */
    if (NULL != gpFILE)
    {
        fprintf(gpFILE, "Program for Lights terminated successfully!!!!\n");
        fclose(gpFILE);
        gpFILE = NULL;
    }
}

void drawObject()
{
    if (bTexture)
    {
        glBindTexture(GL_TEXTURE_2D, uTexture);
    }
    else
    {
        glColor3f(1.0f, 0.0f, 0.0f);
    }
    glBegin(GL_QUADS);

    /* Front face */
    glNormal3f(0.0f, 0.0f, 1.0f);

    glTexCoord2f(0.0f, 0.0f); // right top
    glVertex3f(1.0f, 1.0f, 1.0f);

    glTexCoord2f(1.0f, 0.0f); // left top
    glVertex3f(-1.0f, 1.0f, 1.0f);

    glTexCoord2f(1.0f, 1.0f); // left bottom
    glVertex3f(-1.0f, -1.0f, 1.0f);

    glTexCoord2f(0.0f, 1.0f); // right  bottom
    glVertex3f(1.0f, -1.0f, 1.0f);

    /* Right Face */
    glNormal3f(-1.0f, 0.0f, 0.0f);

    glTexCoord2f(0.0f, 0.0f); // right top
    glVertex3f(1.0f, 1.0f, -1.0f);

    glTexCoord2f(1.0f, 0.0f); // left top
    glVertex3f(1.0f, 1.0f, 1.0f);

    glTexCoord2f(1.0f, 1.0f); // left bottom
    glVertex3f(1.0f, -1.0f, 1.0f);

    glTexCoord2f(0.0f, 1.0f); // right bottom
    glVertex3f(1.0f, -1.0f, -1.0f);

    /* Back Face */
    glTexCoord2f(0.0f, 0.0f); // right top
    glVertex3f(-1.0f, 1.0f, -1.0f);

    glTexCoord2f(1.0f, 0.0f); // left top
    glVertex3f(1.0f, 1.0f, -1.0f);

    glTexCoord2f(1.0f, 1.0f); // left bottom
    glVertex3f(1.0f, -1.0f, -1.0f);

    glTexCoord2f(0.0f, 1.0f); // right bottom
    glVertex3f(-1.0f, -1.0f, -1.0f);

    /* Left face */
    glNormal3f(-1.0f, 0.0f, 0.0f);

    glTexCoord2f(0.0f, 0.0f); // right top
    glVertex3f(-1.0f, 1.0f, 1.0f);

    glTexCoord2f(1.0f, 0.0f); // left top
    glVertex3f(-1.0f, 1.0f, -1.0f);

    glTexCoord2f(1.0f, 1.0f); // left bottom
    glVertex3f(-1.0f, -1.0f, -1.0f);

    glTexCoord2f(0.0f, 1.0f); // right bottom
    glVertex3f(-1.0f, -1.0f, 1.0f);

    /* Top face */
    glNormal3f(0.0f, 1.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f); // right top
    glVertex3f(1.0f, 1.0f, -1.0f);

    glTexCoord2f(1.0f, 0.0f); // left top
    glVertex3f(-1.0f, 1.0f, -1.0f);

    glTexCoord2f(1.0f, 1.0f); // left bottom
    glVertex3f(-1.0f, 1.0f, 1.0f);

    glTexCoord2f(0.0f, 1.0f); // right top
    glVertex3f(1.0f, 1.0f, 1.0f);

    /* Bottom face */
    glNormal3f(0.0f, -1.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f); // right top
    glVertex3f(1.0f, -1.0f, 1.0f);

    glTexCoord2f(1.0f, 0.0f); // left top
    glVertex3f(-1.0f, -1.0f, 1.0f);

    glTexCoord2f(1.0f, 1.0f); // left bottom
    glVertex3f(-1.0f, -1.0f, -1.0f);

    glTexCoord2f(0.0f, 1.0f); // right bottom
    glVertex3f(1.0f, -1.0f, -1.0f);

    glEnd();
    if (bTexture)
    {
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    else
    {
        
        glColor3f(1.0f, 1.0f, 1.0f);
    }
}